import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());

app.get('/skb-2a', (req, res) => {
        const a = parseInt(req.query.a) || 0;
        const b = parseInt(req.query.b) || 0;
        res.send((a+b).toString());
    }
);

app.listen(3000, () => {
    console.log('Your app listening on port 3000!');
});